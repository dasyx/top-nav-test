 function handleWheel(event) {
   const navbarImage = document.querySelector('#cool_image');
   if (!navbarImage) return;

   const currentScrollY = window.scrollY;
  //  const threshold = 100;

   // 根据滚动距离计算缩放比例
   let scale = currentScrollY <= 100 ? 1 : 0.7;

   // 更新图片的transform样式
  //  navbarImage.style.transform = `scale(${scale})`;
   navbarImage.style.transform = `scale(${scale}) translate(0, 0)`;
   navbarImage.style.transformOrigin = 'top';
 }

 document.addEventListener('wheel', handleWheel);